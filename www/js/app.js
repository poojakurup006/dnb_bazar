// Ionic Starter App

angular.module('underscore', [])
.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('dnbbazar', [
  'ionic',
  'angularMoment',
  'multipleDatePicker',
  'dnbbazar.controllers',
  'dnbbazar.directives',
  'dnbbazar.filters',
  'dnbbazar.services',
  'dnbbazar.factories',
  'dnbbazar.config',
  'dnbbazar.views',
  'underscore',
   'ngResource',
  'ngCordova',
  'slugifier',
  'btford.socket-io'
 
])

.run(function($ionicPlatform, PushNotificationsService, $cordovaContacts,CartService,RequestsService,$rootScope, $ionicConfig, $timeout,$state,$http) {
$ionicPlatform.ready(function() {
                    // Enable to debug issues.
                  // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
                  
                  var notificationOpenedCallback = function(jsonData) {
                   // console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
                   // alert(JSON.stringify(jsonData.notification));
                    var newList=localStorage.notification?JSON.parse(localStorage.notification):[];
                    newList.push(jsonData.notification);
                    localStorage.notification=JSON.stringify(newList);
                    $rootScope.$broadcast('new Notification',{});
                  };

                  window.plugins.OneSignal
                    .startInit("8a48d04b-fdab-4321-a282-db644244df96")
                    .handleNotificationOpened(notificationOpenedCallback)
                    .endInit();
                    
                  // Call syncHashedEmail anywhere in your app if you have the user's email.
                  // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
                  // window.plugins.OneSignal.syncHashedEmail(userEmail);

                  window.plugins.OneSignal.getIds(function(ids) {
                        console.log('getIds: ' + JSON.stringify(ids));
                        //alert("userId = " + ids.userId + ", pushToken = " + ids.pushToken);
                        RequestsService.register(ids.pushToken,ids.userId);
                        
                      });
                });
  
$ionicPlatform.registerBackButtonAction(function (event) {
    if($state.current.name=="app.fooditems.vegetableslist" || $state.current.name=="auth.walkthrough" || $state.current.name=='auth.login'){
      navigator.app.exitApp();
    }
    else if($state.current.name=="app.thankyou"){ $state.go('app.fooditems.vegetableslist');}
    else {
      navigator.app.backHistory();
    }
  }, 100);

document.addEventListener("CartChange", function() {
       //code for action on resume
      }, false);
  $ionicPlatform.on("deviceready", function(){
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
 var version = "200";
 var savedAppVersion = localStorage.getItem("version");
  if (savedAppVersion && savedAppVersion === version) {
    // do nothing
  } else {
    /*clear all local storage*/
    localStorage.clear();
    /*save new code to ls*/
    localStorage.setItem("version", version);
  }
    
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }

   

    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    console.log("before push");
    //PushNotificationsService.register();
    console.log("after push");

  });

  // This fixes transitions for transparent background views
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    console.log(JSON.stringify(toState));
   // console.log(localStorage.globals.hasOwnProperty('currentUser'));
    
  
    $rootScope.globals = localStorage.globals|| {currentUser:''};

     console.log($rootScope.globals);
     $rootScope.globals = $rootScope.globals === localStorage.globals? angular.fromJson(localStorage.globals): {currentUser:''};

        if(! $rootScope.globals.hasOwnProperty('currentUser')){ 
       Object.defineProperty( $rootScope.globals,'currentUser',{value:''});}
        console.log('ttt'+ $rootScope.globals.hasOwnProperty('currentUser'));

        if ($rootScope.globals.currentUser!=='' ) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
    var restrictedPage = $.inArray(toState.name, ['auth.walkthrough','auth.otp','auth.login','auth.signup','auth.forgot-password','auth.forgot-password']) === -1;
           console.log("llogout testing  "+JSON.stringify(restrictedPage));
            var loggedIn = $rootScope.globals.currentUser!==''? $rootScope.globals.currentUser: false;
            console.log("llogout testing  "+loggedIn);
            if (restrictedPage && !loggedIn) {
               // $location.path('/login');
               event.preventDefault();

       // $state.transitionTo("app.fooditems", null, {notify:false});
               console.log(JSON.stringify(restrictedPage));
               $state.go('auth.walkthrough');
            }

    if(toState.name.indexOf('auth.walkthrough') > -1)
    {
      // set transitions to android to avoid weird visual effect in the walkthrough transitions
      $timeout(function(){
        $ionicConfig.views.transition('android');
        $ionicConfig.views.swipeBackEnabled(false);
        console.log("setting transition to android and disabling swipe back");
      }, 0);
      if(loggedIn){event.preventDefault();

       // $state.transitionTo("app.fooditems", null, {notify:false});
               console.log(JSON.stringify(restrictedPage));
               $state.go('app.fooditems.vegetableslist');}
    }

  });
  $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams){
    if(toState.name.indexOf('app.fooditems.vegetableslist') > -1)
    {
      // Restore platform default transition. We are just hardcoding android transitions to auth views.
      $ionicConfig.views.transition('platform');
      // If it's ios, then enable swipe back again
      if(ionic.Platform.isIOS())
      {
        $ionicConfig.views.swipeBackEnabled(true);
      }
    	console.log("enabling swipe back and restoring transition to platform default", $ionicConfig.views.transition());
    }
  });

  $ionicPlatform.on("resume", function(){
    //PushNotificationsService.register();
    if(window.plugins.OneSignal){ window.plugins.OneSignal.getIds(function(ids) {
                        console.log('getIds: ' + JSON.stringify(ids));
                        //alert("userId = " + ids.userId + ", pushToken = " + ids.pushToken);
                        RequestsService.register(ids.pushToken,ids.userId);
                        
                      });}
    CartService.CartChange().then(function(res){console.log(res);},function(err){console.log("there is an error");});
    $rootScope.$broadcast('priceChange', { message: "test" });
   
  });

  

})


.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $stateProvider

  //INTRO
  .state('auth', {
    url: "/auth",
    templateUrl: "views/auth/auth.html",
    abstract: true,
    controller: 'AuthCtrl'
  })

  .state('auth.walkthrough', {
    url: '/walkthrough',
    templateUrl: "views/auth/walkthrough.html"
  })

  .state('auth.login', {
    url: '/login',
    templateUrl: "views/auth/login.html",
    controller: 'LoginCtrl'
  })
  .state("auth.logout", {
        url: "/logout",
        templateUrl: "views/auth/logout.html",
        controller: "LogoutCtrl"
      })


  .state('auth.signup', {
    url: '/signup',
    templateUrl: "views/auth/signup.html",
    controller: 'SignupCtrl',
     resolve: {
      States_data: function(AuthService, $ionicLoading) {
         $ionicLoading.show({
            template: 'Loading ...'
          });
          return AuthService.getStates();
      },
      Master_City_data: function(AuthService) {
               return AuthService.getMasterCities();
      },
      Cities_data: function(AuthService) {
               return AuthService.getCities();
      },
      Areas_data: function(AuthService) {
               return AuthService.getAreas();
      },
      Status_data: function(AuthService) {
               return AuthService.getStatus();
      }


    }
  })

  .state('auth.forgot-password', {
    url: "/forgot-password",
    templateUrl: "views/auth/forgot-password.html",
    controller: 'ForgotPasswordCtrl'
  })

  .state('auth.otp', {
    url: "/otp/:mobile",
    templateUrl: "views/auth/otp.html",
    controller: 'oneTimePasswordCtrl'
  })


  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "views/app/side-menu.html",
    controller: 'AppCtrl'
  })

  
  .state('app.support', {
    url: "/support",
    views: {
      'menuContent': {
        templateUrl: "views/app/support.html",
        controller: 'SupportCtrl'

      }
    }
  })

  .state('app.recharge', {
    url: "/recharge",
    views: {
      'menuContent': {
        templateUrl: "views/app/recharge.html",
        controller: 'RechargeWalletCtrl'

      }
    }
  })

  .state('app.notification', {
    url: "/notification",
    views: {
      'menuContent': {
        templateUrl: "views/app/notification.html",
        controller: 'notificationctrl'

      }
    }
  })

  .state('app.balance', {
    url: "/balance",
    views: {
      'menuContent': {
       
        templateUrl: "views/app/balance.html",
        controller: 'BalanceCtrl'
      }
    }
  })

  .state('app.profile', {
    url: "/profile",
    views: {
      'menuContent': {
        templateUrl: "views/app/profile.html",
        controller: 'profileCtrl'

      }
    },resolve: {
      address_data: function(AuthService, $ionicLoading) {
            $ionicLoading.show({
            template: 'Fetching Details ...'
          });
          return AuthService.getAddressData();
      },
      States_data: function(AuthService) {
               return AuthService.getStates();
      },
      Master_City_data: function(AuthService) {
               return AuthService.getMasterCities();
      },
      Cities_data: function(AuthService) {
               return AuthService.getCities();
      },
      Areas_data: function(AuthService) {
               return AuthService.getAreas();
      }

    }
  })

  

 .state('app.fooditems', {
    url: '/fooditems',
    abstract: true,
      views: {
      'menuContent': {
         templateUrl: 'views/app/fooditems/tabsController.html'
    
      
      }
    }
   
   
    
  })

 .state('app.fooditems.vegetableslist', {
    url: '/fooditems/vegetableslist',
    views: {
       'tab1': {
           
        templateUrl: 'views/app/fooditems/vegetableslist4.2.html',
        controller: 'vegetablesListCtrl'
      }  
     
  } 
      
    
  })

  .state('app.fooditems.fruitslist', {
    url: '/fooditems/fruitslist',
    views: {
      'tab2': {
        templateUrl: 'views/app/fooditems/fruitslist4.2.html',
        controller: 'fruitsListCtrl', 
         
              }
          }
  })

  .state('app.fooditems.subscription', {
    url: '/fooditems/subscription',
    views: {
      'tab3': {
        templateUrl: 'views/app/fooditems/subscription.html',
        controller: 'subscriptionListCtrl' 
         
              }
          }
  })

  .state('app.subscription_detail', {
    url: '/fooditems/subscription_detail/:id',
    views: {
      'menuContent': {
        templateUrl: 'views/app/fooditems/subscription_detail.html',
        controller: 'subscriptionDetailCtrl'
         
              }
          }
  })

  .state('app.orders', {
    url: '/orders',
    abstract: true,
      views: {
      'menuContent': {
         templateUrl: 'views/app/orders/ordersController.html'
    
      
      }
    }
   
   
    
  })

 .state('app.orders.currentOrders', {
    url: '/orders/currentOrders',
    views: {
       'tab3': {
         templateUrl: 'views/app/orders/currentOrders.html',
        controller: 'currentOrdersListCtrl'
      }  
     
  } 
    
  })

  .state('app.orders.ordersHistory', {
    url: '/orders/ordersHistory',
    views: {
      'tab4': {
        templateUrl: 'views/app/orders/ordersHistory.html',
        controller: 'ordersHistoryListCtrl'
              
      }
    }
  })
.state("app.orderDetails", {
        url: "/orders/orderDetails/:orderType/:id",
        views: {
            menuContent: {
                templateUrl: "views/app/orders/orderDetails.html",
                controller: "orderDetailsCtrl"
            }
        }
    })

  .state("app.cart", {
        url: "/fooditems/cart",
        views: {
            menuContent: {
                templateUrl: "views/app/fooditems/cart.html",
                controller: "ShoppingCartCtrl"
            }
        }
    })

  .state('app.shipping', {
    url: "/fooditems/shipping",
    views: {
      'menuContent': {
        templateUrl: "views/app/fooditems/shippingaddress.html",
        controller: 'ShippingCtrl'
      }
    },
    resolve: {
      address_data: function(AuthService, $ionicLoading) {
        $ionicLoading.show({
            template: 'Loading ...'
          });
        return AuthService.getAddressData();
      },
      time_data:function(CartService){
         return CartService.getDeliveryTime();
      },
      States_data: function(AuthService) {
               return AuthService.getStates();
      },
      Cities_data: function(AuthService) {
               return AuthService.getCities();
      },
      Areas_data: function(AuthService) {
               return AuthService.getAreas();
      }
    }
  })

  .state("app.thankyou", {
        url: "fooditems/thankyou/:delivery_time/:total",
        views: {
            menuContent: {
                templateUrl: "views/app/fooditems/thankyou.html",
                controller: "ThankyouCtrl"
            }
        }
    })
  
;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/auth/walkthrough');
});
